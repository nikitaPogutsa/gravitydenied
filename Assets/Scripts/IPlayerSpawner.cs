﻿using UnityEngine;

public interface IPlayerSpawner : ILaunch
{
    void SpawnPlayer();
    Vector2 GetInitialPosition();
}