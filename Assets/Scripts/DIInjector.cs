﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//Primitive DI injector
public class DIInjector
{
    public void InjectDependencies(params object[] gameSystems)
    {
        foreach (var _class in gameSystems)
        {
            var methods = _class.GetType().GetMethods().Where(x => x.GetCustomAttributes(typeof(InjectAttribute), true).Count() > 0).ToList();
            foreach (var item in methods)
            {
                var parameters = item.GetParameters();
                List<object> toResolve = new List<object>();
                foreach (var parameter in parameters)
                {
                    try
                    {
                        toResolve.Add(gameSystems.First(y => y.GetType().GetInterfaces().Contains(parameter.ParameterType) || y.GetType() == parameter.ParameterType));
                    }
                    catch (Exception)
                    {
                        Debug.LogError($"Error caught while resolving {parameter.ParameterType} for {_class}");
                    }
                }
                item.Invoke(_class, toResolve.ToArray());
            }
        }
    }
}