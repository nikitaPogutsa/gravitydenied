﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[RequireComponent(typeof(EdgeCollider2D))]
public class TerrainGenerator : MonoBehaviour, IDisposable, ILaunch
{
    [SerializeField] private float additionalSeed = 1.2f;
    [SerializeField] private int cachedPoints = 15;
    [SerializeField] private float terrainScale = 7;
    [SerializeField] float ySeed;
    [SerializeField] float xSeed;
    private EdgeCollider2D edgeCollider;
    private Vector2[] newCol;
    private Vector2 noise = new Vector2(1.1f, 1.1f);

    private void Awake()
    {
        edgeCollider = GetComponent<EdgeCollider2D>();
    }

    public void GenerateTerrain()
    {
        newCol = new Vector2[cachedPoints];
        noise = new Vector2(1.1f, 1.1f);
        for (int i = 1; i < newCol.Length; i++)
        {
            newCol[i] = GeneratePerlinPointRandom();
        }
        edgeCollider.points = newCol;
    }

    public void GenerateEdge()
    {
        int cutoff = (int)(edgeCollider.pointCount * 0.3f);
        Vector2[] tempPoints = edgeCollider.points;

        for (int j = 0; j < cutoff; j++)
        {
            for (int i = 0; i < edgeCollider.pointCount - 1; i++)
            {
                tempPoints[i] = tempPoints[i + 1];
            }
        }
        for (int i = edgeCollider.pointCount - cutoff; i < edgeCollider.pointCount; i++)
        {
            tempPoints[i] = GeneratePerlinPointRandom();
        }
        edgeCollider.points = tempPoints;
    }

    private Vector2 GeneratePerlinPointRandom()
    {
        //noise = new Vector2(noise.x += xSeed, noise.);
        var perlinY = Mathf.PerlinNoise(noise.x += xSeed, noise.y += ySeed) * UnityEngine.Random.Range(-additionalSeed, additionalSeed);
        return new Vector2(noise.x, perlinY) * terrainScale;
    }

    public void Dispose()
    {
        edgeCollider.points = new Vector2[0];
    }

    public void Launch()
    {
        GenerateTerrain();
    }
}
