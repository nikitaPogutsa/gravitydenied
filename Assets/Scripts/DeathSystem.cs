﻿using UnityEngine;

public class DeathSystem: MonoBehaviour, IDeathSystem
{
    private UIController uiController;
    private GameManager gameManager;

    [Inject]
    public void Instantiate( UIController uiController, GameManager gameManager)
    {
        this.uiController = uiController;
        this.gameManager = gameManager;
    }


    public void Die()
    {
        uiController.DisplayText(TextDisplayEnum.crashed);
        gameManager.Restart();
    }
}