﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class InputManagementSystemPointer : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IInputManagementSystem
{
    public bool isLeftButtonDown
    {
        get;
        set;
    }
    public bool isRightButtonDown
    {
        get;
        set;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Camera.main.ScreenToViewportPoint(eventData.pressPosition).x < 0.5f)
        {
            isLeftButtonDown = true;
            return;
        }
        isRightButtonDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isLeftButtonDown = false;
        isRightButtonDown = false;
    }

    private void OnGUI()
    {
        //   GUI.TextField(new Rect(0, 20, 100, 20), $"Left {isLeftButtonDown.ToString()}");
        //   GUI.TextField(new Rect(0, 40, 100, 20), $"Right {isRightButtonDown.ToString()}");
    }
}
