﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceObserver : MonoBehaviour, IDisposable
{
    private UIController uiController;
    private IPlayerSpawner playerSpawner;
    private IMovementController movementController;

    [Inject]
    public void Initialize(UIController uIController, IPlayerSpawner playerSpawner, IMovementController movementController)
    {
        this.uiController = uIController;
        this.playerSpawner = playerSpawner;
        this.movementController = movementController;
    }

    public void Update()
    {
        float distance = (movementController.position - playerSpawner.GetInitialPosition()).x;
        uiController.DisplayDistance(Mathf.Clamp(Mathf.FloorToInt(distance), 0, int.MaxValue));
    }

    public void Dispose()
    {
        uiController.DisplayDistance(0);
    }
}
