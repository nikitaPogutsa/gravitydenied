﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(EdgeCollider2D))]
public class TerrainScrollObserver : MonoBehaviour, IDisposable, ILaunch
{
    [SerializeField] private float scrollCutoff = 0.5f;
    [SerializeField] private float checkFrequency = 2f;
    private WaitForSeconds checkFrequencyAwaiter;
    private IMovementController movementController;
    //No Dependency Inversion  by choice
    private TerrainGenerator terrain;
    private TerrainVisualizer terrainVisualizer;
    private Coroutine observePlayerCoroutine;
    private EdgeCollider2D edgeCollider2D;

    public void Dispose()
    {
        StopCoroutine(observePlayerCoroutine);
    }
    [Inject]
    public void Initialize(IMovementController movementController, TerrainGenerator terrain, TerrainVisualizer terrainVisualizer)
    {
        this.movementController = movementController;
        this.terrain = terrain;
        this.terrainVisualizer = terrainVisualizer;
        checkFrequencyAwaiter = new WaitForSeconds(checkFrequency);
        edgeCollider2D = GetComponent<EdgeCollider2D>();
    }

    public void Launch()
    {
        observePlayerCoroutine = StartCoroutine(ObservePlayerScrollPosition());
    }

    private IEnumerator ObservePlayerScrollPosition()
    {
       // yield return new WaitForSeconds(1);
        while (true)
        {
            if (edgeCollider2D.pointCount > 0)
            {
                var index = Array.IndexOf<Vector2>(edgeCollider2D.points, edgeCollider2D.points.OrderBy(x =>
                    {
                        return Vector2.Distance(movementController.position, transform.TransformPoint(x));
                    }).FirstOrDefault());
                if ((float)index / (float)edgeCollider2D.pointCount > scrollCutoff)
                {
                    terrain.GenerateEdge();
                    terrainVisualizer.DrawTerrain();
                }
            }
            yield return checkFrequencyAwaiter;
        }
    }
}

