﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StuntObserver : MonoBehaviour, IDisposable
{
    private UIController uiController;
    private ITerrainContactPointObserver terrainContactPointObserver;
    private float wheelieTime;
    private bool isMidWheelie;

    [Inject]
    public void Initialize(UIController uiController, ITerrainContactPointObserver terrainContactPointObserver)
    {
        this.uiController = uiController;
        this.terrainContactPointObserver = terrainContactPointObserver;
    }

    private void ResetWheelie()
    {
        isMidWheelie = false;
        wheelieTime = 0;
    }

    private void Update()
    {
        if (!terrainContactPointObserver.isFrontWheelGrounded)
        {
            if (terrainContactPointObserver.isRearWheelGrounded)
            {
                isMidWheelie = true;
                wheelieTime += Time.deltaTime;
                if (wheelieTime > 1)
                {
                    uiController.DisplayText(TextDisplayEnum.wheelie);
                    ResetWheelie();
                }
            }
        }
        else
        {
            ResetWheelie();
        }
    }

    public void Dispose()
    {
        ResetWheelie();
    }
}
