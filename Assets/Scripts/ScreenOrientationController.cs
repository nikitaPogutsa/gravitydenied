﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenOrientationController : MonoBehaviour
{
    private ICameraSystem cameraSystem;
    private ScreenOrientation cachedScreenOrientation = ScreenOrientation.AutoRotation;

    [Inject]
    public void Initialize(ICameraSystem cameraSystem)
    {
        this.cameraSystem = cameraSystem;
    }

    private void Update()
    {
        if (Screen.orientation != cachedScreenOrientation)
        {
            if (Screen.orientation == ScreenOrientation.Landscape)
            {
                cameraSystem.cameraSize = 20;
            }
            else
            {
                cameraSystem.cameraSize = 38;
            }
        }
    }
}
