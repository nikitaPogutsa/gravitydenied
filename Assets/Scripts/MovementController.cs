﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]
public class MovementController : MonoBehaviour, IMovementController, ILaunch
{
    [SerializeField] private float speedLimit = 13000f;
    [SerializeField] private Rigidbody2D backTire;
    [SerializeField] private Rigidbody2D chassis;
    [SerializeField] float breakingDampRate = 7;
    [SerializeField] private float speed = 500;
    [SerializeField] private float idleDampRate = 25;
    private IInputManagementSystem inputManagementSystem;
    private ITerrainContactPointObserver terrainContactPointObserver;
    private List<Rigidbody2D> rigidbodies = new List<Rigidbody2D>();
    private float velocity;
    public Vector2 position { get => this.transform.position; set => this.transform.position = value; }

    [Inject]
    public void Initialize(IInputManagementSystem inputManagementSystem, ITerrainContactPointObserver terrainContactPointObserver)
    {
        this.inputManagementSystem = inputManagementSystem;
        this.terrainContactPointObserver = terrainContactPointObserver;
    }

    private void Start()
    {
        rigidbodies.AddRange(GetComponentsInChildren<Rigidbody2D>());
        rigidbodies.Add(GetComponent<Rigidbody2D>());
    }

    private void Update()
    {

        if (inputManagementSystem.isRightButtonDown)
        {
            if (terrainContactPointObserver.isRearWheelGrounded)
            {
                backTire.AddTorque(-speed);
                backTire.angularVelocity = Mathf.Clamp(backTire.angularVelocity, -speedLimit, speedLimit);
            }
        }
        else if (inputManagementSystem.isLeftButtonDown)
        {
            if (terrainContactPointObserver.isRearWheelGrounded)
            {
                backTire.angularDrag = Mathf.Lerp(backTire.angularDrag, 0, breakingDampRate);
                chassis.velocity = Vector3.Lerp(chassis.velocity, chassis.velocity.normalized, Time.deltaTime * breakingDampRate);
            }
            chassis.angularVelocity -= chassis.angularDrag * breakingDampRate * 20 * Time.deltaTime;
        }
        else
        {
            backTire.angularVelocity = Mathf.SmoothDamp(backTire.angularVelocity, 0, ref velocity, Time.deltaTime * idleDampRate);
        }

    }

#if UNITY_EDITOR
    private void OnGUI()
    {
        // GUI.TextField(new Rect(0, 0, 100, 20), backTire.angularVelocity.ToString());
    }
#endif

    public void Launch()
    {
        foreach (var item in rigidbodies)
        {
            item.simulated = true;
        }
    }

    public void Dispose()
    {
        transform.rotation = Quaternion.identity;
        foreach (var item in rigidbodies)
        {
            item.angularVelocity = 0;
            item.velocity = new Vector2();
            item.rotation = 0;
            item.simulated = false;
        }
    }
}
