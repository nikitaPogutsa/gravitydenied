﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private Text displayText;
    [SerializeField] private Text distanceText;
    [SerializeField] private float textLifetime;
    [SerializeField] private float textFadeRate;
    private Coroutine displayTextCoroutine;
    private WaitForSeconds textLifetimeAwaiter;
    private WaitForSeconds textFadeRateAwaiter;
    private WaitForEndOfFrame endOfFrameAwaiter;

    private void Start()
    {
        textLifetimeAwaiter = new WaitForSeconds(textLifetime);
        textFadeRateAwaiter = new WaitForSeconds(textFadeRate);
        endOfFrameAwaiter = new WaitForEndOfFrame();
    }

    public void DisplayText(TextDisplayEnum textDisplayEnum)
    {
        displayText.color = Color.white;
        displayText.text = textDisplayEnum.ToString();
        if (displayTextCoroutine != null)
        {
            StopCoroutine(displayTextCoroutine);
        }
        displayTextCoroutine = StartCoroutine(FadeTextCoroutine());
    }

    public void DisplayDistance(int distance)
    {
        distanceText.text = distance.ToString();
    }

    private IEnumerator FadeTextCoroutine()
    {
        yield return textLifetimeAwaiter;
        for (float i = 0; i < 1; i += textFadeRate)
        {
            displayText.color = new Color(displayText.color.r, displayText.color.g, displayText.color.b, displayText.color.a - textFadeRate);
            yield return endOfFrameAwaiter;
        }
    }

}
