﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<UnityEngine.Object> gameSystems;
    private List<IDisposable> disposableGroup = new List<IDisposable>();
    private List<ILaunch> launchableGroup = new List<ILaunch>();

    void Start()
    {
        ResolveDependencies();
        launchableGroup.ForEach(x => x.Launch());
    }

    void ResolveDependencies()
    {
        //Some classes are left coupled and not abstracted intentionally due to time limitations
        DIInjector dIInjector = new DIInjector();
        dIInjector.InjectDependencies(gameSystems.ToArray());
        PopulateSystemGroups();
    }

    private void PopulateSystemGroups()
    {
        foreach (var item in gameSystems)
        {
            if (item.GetType().GetInterfaces().Contains((typeof(IDisposable))))
            {
                disposableGroup.Add((IDisposable)item);
            }
            if (item.GetType().GetInterfaces().Contains((typeof(ILaunch))))
            {
                launchableGroup.Add((ILaunch)item);
            }
        }
    }

    public void Restart()
    {
        disposableGroup.ForEach(x => x.Dispose());
        launchableGroup.ForEach(x => x.Launch());
    }
}
