﻿using System;
using UnityEngine;

public interface IMovementController : IDisposable
{
    Vector2 position { get; set; }
    void Launch();
}