﻿using UnityEngine;

public class InputManagementSystemTouchControl : MonoBehaviour, IInputManagementSystem
{
    public bool isLeftButtonDown
    {
        get;
        set;
    }
    public bool isRightButtonDown
    {
        get;
        set;
    }

    public void Update()
    {
        var touches = Input.touches;
        isLeftButtonDown = false;
        isRightButtonDown = false;
        for (int i = 0; i < touches.Length; i++)
        {
            if (Camera.main.ScreenToViewportPoint(touches[i].position).x < 0.5f)
            {
                isLeftButtonDown = true;
                continue;
            }
            isRightButtonDown = true;
        }
    }
}

