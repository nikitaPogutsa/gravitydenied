﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EdgeCollider2D))]
[RequireComponent(typeof(LineRenderer))]
public class TerrainVisualizer : MonoBehaviour, IDisposable, ILaunch
{
    [SerializeField] private Vector2 widthOffset;
    [SerializeField] private Vector2 heightOffset;
    private EdgeCollider2D edgeCollider;
    private LineRenderer lineRenderer;
    private delegate void DrawPattern();
    private DrawPattern meshPattern;

    private void Awake()
    {
        edgeCollider = GetComponent<EdgeCollider2D>();
        lineRenderer = GetComponent<LineRenderer>();
        meshPattern = DrawMeshPattern;
    }

    private void DrawMeshPattern()
    {
        var pointsCount = edgeCollider.pointCount;
        lineRenderer.positionCount = (pointsCount * 8) - 8;
        int rendererPointIndex = 0;
        for (int i = 0; i < pointsCount - 1; i++)
        {
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] - widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] + widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i + 1] + widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i + 1] - widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] - widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, new Vector2(edgeCollider.points[i].x, heightOffset.y));
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, new Vector2(edgeCollider.points[i + 1].x, heightOffset.y));
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i + 1] - widthOffset);
            rendererPointIndex++;

        }
    }

    private void DrawTriangularPattern()
    {
        var pointsCount = edgeCollider.pointCount;
        lineRenderer.positionCount = (pointsCount * 4) - 4;
        int rendererPointIndex = 0;
        for (int i = 0; i < pointsCount - 1; i++)
        {
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] - widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] + widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i + 1] + widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] - widthOffset);
            rendererPointIndex++;
        }
    }

    private void DrawTilePattern()
    {
        var pointsCount = edgeCollider.pointCount;
        lineRenderer.positionCount = (pointsCount * 6) - 6;
        int rendererPointIndex = 0;
        for (int i = 0; i < pointsCount - 1; i++)
        {
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] - widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] + widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i + 1] + widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i + 1] - widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i] - widthOffset);
            rendererPointIndex++;
            lineRenderer.SetPosition(rendererPointIndex, edgeCollider.points[i + 1] - widthOffset);
            rendererPointIndex++;
        }
    }

    public void DrawTerrain()
    {
        meshPattern();
    }

    public void Dispose()
    {
        lineRenderer.positionCount = 0;
    }

    public void Launch()
    {
        DrawTerrain();
    }
}
