﻿public interface ITerrainContactPointObserver
{
    bool isFrontWheelGrounded { get; }
    bool isRearWheelGrounded { get; }
}