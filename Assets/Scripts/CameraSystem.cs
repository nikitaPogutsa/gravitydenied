﻿using UnityEngine;
using System.Collections;

public class CameraSystem : MonoBehaviour, ICameraSystem
{
    [SerializeField] private Camera sceneCamera;
    [SerializeField] private Transform target;
    [SerializeField] private float smoothRate = 0.3f;
    private Vector3 velocity = Vector3.zero;
    private float smoothTime = 15;

    public float cameraSize { set => sceneCamera.orthographicSize = value; }

    public void UpdateCameraState()
    {
        sceneCamera.transform.position = Vector3.Lerp(sceneCamera.transform.position, new Vector3(target.position.x, sceneCamera.transform.position.y, sceneCamera.transform.position.z), Time.deltaTime * smoothTime);
    }

    void Update()
    {
        UpdateCameraState();
    }
}

public interface ICameraSystem
{
    void UpdateCameraState();
    float cameraSize { set; }
}