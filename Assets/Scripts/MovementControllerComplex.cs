﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]
public class MovementControllerComplex : MonoBehaviour, IMovementController
{
    [SerializeField] private float speedLimit = 13000f;
    [SerializeField] private Rigidbody2D backTire;
    [SerializeField] private Rigidbody2D chassis;
    [SerializeField] private float speed = 500;
    private IInputManagementSystem inputManagementSystem;
    private ITerrainContactPointObserver terrainContactPointObserver;
    private float velocity;
    private List<Rigidbody2D> rigidbodies = new List<Rigidbody2D>();
    public Vector2 position { get => this.transform.position; set => this.transform.position = value; }

    [Inject]
    public void Initialize(IInputManagementSystem inputManagementSystem, ITerrainContactPointObserver terrainContactPointObserver)
    {
        this.inputManagementSystem = inputManagementSystem;
        this.terrainContactPointObserver = terrainContactPointObserver;
    }

    private void Start()
    {
        rigidbodies.AddRange(GetComponentsInChildren<Rigidbody2D>());
        rigidbodies.Add(GetComponent<Rigidbody2D>());
    }

    private void Update()
    {
        if (inputManagementSystem.isRightButtonDown && !inputManagementSystem.isLeftButtonDown)
        {
            if (terrainContactPointObserver.isRearWheelGrounded)
            {
                backTire.angularDrag = 0.05f;
                backTire.AddTorque(-speed);
                backTire.angularVelocity = Mathf.Clamp(backTire.angularVelocity, -speedLimit, speedLimit);
            }
            chassis.AddTorque(500);
        }
        else if (inputManagementSystem.isLeftButtonDown && !inputManagementSystem.isRightButtonDown)
        {
            backTire.angularDrag = Mathf.Lerp(backTire.angularDrag, 0, 25);
            if (terrainContactPointObserver.isRearWheelGrounded)
            {
                chassis.velocity = Vector3.Lerp(chassis.velocity, chassis.velocity.normalized, Time.deltaTime * 5);
            }
            chassis.AddTorque(-500);
        }
        else if (inputManagementSystem.isRightButtonDown && inputManagementSystem.isLeftButtonDown)
        {
            if (terrainContactPointObserver.isRearWheelGrounded)
            {
                backTire.angularDrag = 0.05f;
                backTire.AddTorque(-speed);
                backTire.angularVelocity = Mathf.Clamp(backTire.angularVelocity, -speedLimit, speedLimit);
            }
        }
        else
        {
            backTire.angularVelocity = Mathf.SmoothDamp(backTire.angularVelocity, 0, ref velocity, Time.deltaTime * 25);
        }
    }

    public void Launch()
    {

        foreach (var item in rigidbodies)
        {
            item.simulated = true;

        }
    }

    public void Dispose()
    {
        transform.rotation = Quaternion.identity;
        foreach (var item in rigidbodies)
        {
            item.angularVelocity = 0;
            item.velocity = new Vector2();
            item.rotation = 0;
            item.simulated = false;
        }

    }
}
