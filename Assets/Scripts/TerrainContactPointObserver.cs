﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[RequireComponent(typeof(EdgeCollider2D))]
public class TerrainContactPointObserver : MonoBehaviour, ITerrainContactPointObserver
{
    private EdgeCollider2D edgeCollider;
    private IDeathSystem deathSystem;
    private int fWHeelMask;
    private int rWheelMask;
    private int chassisMask;
    public bool isFrontWheelGrounded => edgeCollider.IsTouchingLayers(fWHeelMask);
    public bool isRearWheelGrounded => edgeCollider.IsTouchingLayers(rWheelMask);
    
    [Inject]
    public void Instantiate(IDeathSystem deathSystem)
    {
        this.deathSystem = deathSystem;
    }

    void Start()
    {
        fWHeelMask = LayerMask.GetMask("FWheel");
        rWheelMask = LayerMask.GetMask("RWheel");
        chassisMask = LayerMask.GetMask("Chassis");
        edgeCollider = GetComponent<EdgeCollider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (edgeCollider.IsTouchingLayers(chassisMask))
        {
            deathSystem.Die();
        }
    }

}
