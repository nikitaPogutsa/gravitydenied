﻿using UnityEngine;

//This doesn't have to inherit MonoBehaviour but I kept it for readability and consistency
public class PlayerSpawner : MonoBehaviour, IPlayerSpawner
{
    [SerializeField] private Vector2 initialPosition;
    private IMovementController movementController;

    [Inject]
    public void Initialize(IMovementController movementController)
    {
        this.movementController = movementController;
    }

    public Vector2 GetInitialPosition()
    {
        return initialPosition;
    }

    public void SpawnPlayer()
    {
        movementController.position = initialPosition;
    }

    public void Launch()
    {
        SpawnPlayer();
    }
}