﻿public interface IInputManagementSystem
{
   bool isLeftButtonDown {get;set;}
   bool isRightButtonDown {get;set;}
}